(function () {
    unfinishedJobsCounts = [];
    finishedJobsCounts = [];
    idleGearsCounts = [];
    unfinishedRatios = [];
    unfinishedToNonIdles = [];
    timestamps = [];

    var data = {
        labels: timestamps,
        datasets: [{
            label:'Finished Jobs : Clients',
            data: finishedJobsCounts
        },{
            label:"Unfinished Jobs : Clients",
            data: unfinishedRatios,
            backgroundColor:'#FFF8F0'
        }, {
            label: 'Raw Unfinished Jobs',
            data: unfinishedJobsCounts,
            backgroundColor:'#F0F8FF'
        }]
    }
    var chartInstance = new Chart(document.getElementById("unfinishedChart"), {
        type: 'line',
        data: data,
        options: {
            title: {
                display: true,
                text: 'Unfinshed Jobs over Time'
            },
            responsive:false
        }
    })

    var idleData = {
        labels: timestamps,
        datasets: [{
            label: "Non-idle Gear : Unfinshed Jobs",
            data: unfinishedToNonIdles
        },{
            label: "Idle Gear : Total Gear",
            data: idleGearsCounts,
            backgroundColor:'#F0F8FF'
        }]
    }
    var idleChart = new Chart(document.getElementById("idleChart"), {
        type: 'line',
        data: idleData,
        options: {
            title: {
                display: true,
                text: 'Ratio of idle gear to used gear over time'
            },
            responsive:false
        }
    })

    Rx.Observable
    .interval(3000)
    .flatMap(function (l) {
        return Rx.Observable.fromCallback($.get)("http://localhost:3002",{});
    })
    .subscribe(function (next) {
        if (next[1] === 'success') {
            update(next[0]);
        }
    });

    function update(data) {
        $(".client_container").empty();
        $(".gear_container").empty();
        var countContainer = {f:0, u:0}
        for (var i = 0; i < data.clients.length; i++) {
            $(".client_container").append("<div class='client'>"+buildClientDiv(data.clients[i], countContainer)+"</div>");
        }
        unfinishedJobsCounts.push(countContainer.u);
        finishedJobsCounts.push(countContainer.f / data.clients.length);
        unfinishedRatios.push(countContainer.u / data.clients.length);
        var idleCountContainer = {i:0};
        for (var i = 0; i < data.gears.length; i++) {
            $(".gear_container").append("<div class='client'>"+buildGearDiv(data.gears[i], idleCountContainer)+"</div>");
        }
        var idleRatio = idleCountContainer.i / data.gears.length;
        idleGearsCounts.push(idleRatio);
        unfinishedToNonIdles.push((data.gears.length - idleCountContainer.i) / countContainer.u);
        var date = new Date();
        var str = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        timestamps.push(str);
        idleChart.update();
        chartInstance.update();
    }

    function buildGearDiv(gear, countContainer) {
        var str = "<p><H2>Gear: " + gear.id + "</H2><br>";
        if (gear.fileSaved >= 0) {
            var timeRemaining = (gear.processingTime - (new Date().getTime() - gear.fileSaved))/ 1000;
            str +="<font color='green'>"+ timeRemaining + " remaining</font><br>"
        } else {
            countContainer.i++;
            str += "<font color='red'>IDLE</font><br>";
        }
        str += '</p>';
        return str;
    }

    function buildClientDiv(client, countContainer) {
        var str = "<p><H2>Client: " + client.id + "</H2><br>";
        var finishedCount = 0;
        var unfinishedCount = 0;
        for (var i = 0; i < client.jobs.length; i++) {
            if (client.jobs[i].done) {
                finishedCount++;
            } else {
                unfinishedCount++;
            }
        }
        countContainer.u += unfinishedCount;
        countContainer.f += finishedCount;
        str += "<font color='green'>"+ finishedCount + " finished jobs</font><br>";
        str += "<font color='red'>"+ unfinishedCount + " unfinshed jobs</font><br>";
        str += "</p>";
        return str;
    }
})()
