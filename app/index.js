var MockGear = require('./model/mock_gear');
var MockClient = require('./model/mock_client');
var Rx = require('rx');
var fs = require('fs');

// where is the backend?
// const HOST = "young-retreat-95370.herokuapp.com";
const HOST = "localhost";
// const PORT_NO = 80;
const PORT_NO = 5000;
// port which you can query for visualizer info
const SERVER_PORT = 3002;
// number of clients to simulate
const NUM_CLIENTS = 12;
// number of pieces of studio gear to simulate
const NUM_GEAR = 24;
// rate at which we cycle through gear asking for new queued files/pushing up completed files
const GEAR_LOOP_INTERVAL_MILLIS = 100;
// rate at which we cycle through clients to push up new files/pull down completed files
const CLIENT_LOOP_INTERVAL_MILLIS = 500;

if (!fs.existsSync(__dirname + "/files")) {
    fs.mkdirSync(__dirname + "/files");
}

var gearDefs = ["Urei1176", "API550A", "DBX530", "GML8200", "API2500", "LexiconPCM92"];

// simulated gear/clients stored in these
var gears = [];
var clients = [];

Rx.Observable
    .range(1, NUM_CLIENTS)
    .map(function (index) {
        clients.push(new MockClient(index))
    })
    .subscribe();

// first, register all your gear
Rx.Observable
    .range(1, NUM_GEAR)
    .map(function (index) {
        var gearDef = gearDefs[index % gearDefs.length];
        return new MockGear(index, index, gearDef, "gear" + index, "100.0.0." + index);
    })
    .doOnNext(function (gear) {
        gears.push(gear);
    })
    .flatMap(function (gear) {
        return Rx.Observable.fromCallback(gear.register.bind(gear))(HOST, PORT_NO);
    })
    .subscribe(function (next) {

    }, function (error) {
        console.error(error);
    }, function () {
        // kick off run loops when all gear registered
        kickOffGearLoop();
        kickOffClientLoop();
        hostInfo();
    });

// build server for visualizer
function hostInfo() {
    const Express = require('express');
    const SERVER = Express();

    SERVER.use('/', function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        next();
    });

    SERVER.get('/', function(request, response){
        // send current state of all gear and clients
        response.status(200).send({"gears": gears, "clients" : clients.map(function (client) {
            return client.prettyVersion();
        })});
    });

    SERVER.listen(SERVER_PORT, function (err) {
        if (err) {
            return console.log('something bad happened', err);
        }
        console.log(`server is listening on ${SERVER_PORT}`);
    });
}

function kickOffGearLoop() {
    Rx.Observable
        .interval(GEAR_LOOP_INTERVAL_MILLIS)
        // "loop" through all gear
        .map(function (index) {
            index = (index % gears.length);
            return gears[index];
        })
        .flatMap(function (gear) {
            // will return true if processing of current job is complete.  Based on passage of time.
            if (gear.shouldUploadNewFile()) {
                return Rx.Observable.fromCallback(gear.uploadProcessed.bind(gear))(HOST, PORT_NO, __dirname + "/files/");
                // returns true if this gear is ready to process a new file
            } else if (gear.shouldLookForNextJob()) {
                return Rx.Observable.fromCallback(gear.fetchNextJob.bind(gear))(HOST, PORT_NO, __dirname + "/files/");
            }
            // case when gear is in the middle of processing
            return Rx.Observable.empty();
        })
        .subscribe(function (next) {
            // console.log(next);
        }, function (error) {
            console.error(error);
        })

}



function kickOffClientLoop() {
    Rx.Observable
        .interval(CLIENT_LOOP_INTERVAL_MILLIS)
        // "loop" through clients
        .map(function (index) {
            index = (index % clients.length);
            return clients[index];
        })
        .flatMap(function (client) {
            // there is a 25% chance this returns true, randomly
            if (client.hasNewJob()) {
                // pick a random gearDef, and upload a file to be processed by one of those
                var randomIndex = parseInt((Math.random() * 300) % gearDefs.length);
                var gearDefId = gearDefs[randomIndex];
                console.log("CLIENT uploading new file of type " + gearDefId + " from client " + client.id);
                return Rx.Observable.fromCallback(client.uploadNewJob.bind(client))(HOST, PORT_NO, gearDefId);
            }
            return Rx.Observable.just(-1);
        }, function (client, code) {
            // we want to just pass the client through
            return client;
        })
        .flatMap(function (client) {
            // does this client have any outstanding jobs? if so, see how they're doing
            var obs = client.getIncompleteJobIds().map(function (jobId) {
                return Rx.Observable.fromCallback(client.downloadCompletedJob.bind(client))(HOST, PORT_NO, jobId);
            });
            return Rx.Observable.from(obs);
        })
        .flatMap(function (obs) {
            return obs;
        })
        .subscribe(function (next) {
            // console.log(next);
        }, function (error) {
            console.error(error);
        })

}