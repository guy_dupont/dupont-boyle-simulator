var http = require('http');
var request = require('request');
var fs = require('fs');

const SHOULD_PROCESS_AUDIO = true;

function MockGear(id, studioId, gearDefId, name, mockAddress) {
    this.id = String(id);
    this.studioId = String(studioId);
    this.gearDefId = gearDefId;
    this.name = name;
    this.mockAddress = mockAddress;
    this.fileSaved = -1;
    this.processingTime = 14000;
}

MockGear.prototype.register = function (hostUrl, portNo, callback) {
    var pathStr = "/register/?studio_id=" + this.studioId + "&id=" + this.id + "&gear_def_id=" + this.gearDefId + "&name=" + this.name;
    var options = {
        host: hostUrl,
        path: pathStr,
        port: portNo,
        method: "POST",
        headers: {
            'x-forwarded-for': this.mockAddress
        }
    };

    var req = http.request(options, function (response) {
        var str = '';
        response.on('data', function (chunk) {
            str += chunk;
        });

        response.on('end', function () {
            callback(str);
        });
    });
    req.on('error', function (e) {
        console.log(e);
    });
    req.end();
};

MockGear.prototype.uploadProcessed = function (hostUrl, portNo, directory, callback) {
    var self = this;
    var formData = {
        // Pass data via Stream
        audio: fs.createReadStream(self.outputFileName)
    };
    request.post({
        url: "http://" + hostUrl + ":" + portNo + "/uploadCompleted/" + self.requestFileName,
        formData: formData
    }, function optionalCallback(err, httpResponse, body) {
        if (!err) {
            console.log("GEAR uploaded finished file for gear" + self.id);
            self.fileSaved = -1;
        } else {
            console.log(err);
        }
        callback(httpResponse.statusCode)
    });
};

MockGear.prototype.shouldUploadNewFile = function () {
    return this.fileSaved > 0 && (new Date().getTime() - this.fileSaved) > this.processingTime;
};

MockGear.prototype.shouldLookForNextJob = function () {
    return this.fileSaved < 0;
};

MockGear.prototype.fetchNextJob = function (hostUrl, portNo, directory, callback) {
    var self = this;
    var pathStr = "/nextJob/" + self.id;
    var options = {
        host: hostUrl,
        path: pathStr,
        port: portNo,
        method: "GET",
        headers: {
            'x-forwarded-for': this.mockAddress
        }
    };

    var req = http.request(options, function (response) {
        if (response.statusCode == 200) {
            self.requestFileName = response.headers['content-disposition'].split('\"')[1];
            self.filename = directory + self.requestFileName;
            var file = fs.createWriteStream(self.filename);
            response.pipe(file);
            self.fileSaved = new Date().getTime();
            console.log("GEAR downloaded file for gear " + self.id);
        }
        response.on('end', function () {
            if (SHOULD_PROCESS_AUDIO) {
                self.processAudio(self.filename, response.statusCode, callback);
            } else {
                callback(response.statusCode);
            }
        });

    });
    req.end();

};

MockGear.prototype.processAudio = function (filePath, statusCode, callback) {
    var exec = require('child_process').exec;
    this.outputFileName = filePath.split('.wav')[0] + '_.wav';
    var cmd = __dirname + "/../../sox/sox " + filePath + " " + this.outputFileName + " reverb";

    exec(cmd, function (error, stdout, stderr) {
        callback(statusCode)
    });
};


module.exports = MockGear;