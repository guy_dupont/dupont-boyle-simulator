var request = require('request');
var fs = require('fs');
// just use the one small one for now
const SEED_FILE_NAMES = ["BoxKick02.wav"];

function MockClient(id) {
    this.id = String(id);
    this.jobMap = {};
    this.mockAddress = "101.0.0." + this.id;
}

MockClient.prototype.hasNewJob = function () {
    return Math.random() < 0.25;
};

MockClient.prototype.downloadCompletedJob = function (hostUrl, portNo, jobId, callback) {
    var self = this;
    var url = "http://" + hostUrl + ":" + portNo + "/completedFile/" + jobId;
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("CLIENT successfully downloaded processed file for job: " + jobId + " for client " + self.id);
            self.jobMap[jobId] = true;
        }
        callback(response.statusCode);
    })
};

MockClient.prototype.prettyVersion = function () {
    var ids = [];
    for (var jobId in this.jobMap) {
        if (this.jobMap.hasOwnProperty(jobId)) {
            var isDone = this.jobMap[jobId];
            ids.push({"jobId" : jobId, "done" : isDone})
        }
    }
    return {"id" : this.id, "jobs" : ids};
};

MockClient.prototype.getIncompleteJobIds = function () {
    var ids = [];
    for (var jobId in this.jobMap) {
        if (this.jobMap.hasOwnProperty(jobId)) {
            if (!this.jobMap[jobId]) {
                ids.push(jobId);
            }
        }
    }
    return ids;
};

MockClient.prototype.uploadNewJob = function (hostUrl, portNo, gearDefId, callback) {
    var self = this;
    var randomIndex = parseInt(Math.random() * 300) % SEED_FILE_NAMES.length;
    var randomSeedFileName = SEED_FILE_NAMES[randomIndex];
    var formData = {
        // Pass data via Streams
        audio: fs.createReadStream(__dirname + "/../seed_files/" + randomSeedFileName)
    };
    request.post({
        url: "http://" + hostUrl + ":" + portNo + "/upload",
        qs: {"gear_def_id" : gearDefId, "name" : self.id + "file", "user_id" : self.id},
        formData: formData,
        headers: {
            'x-forwarded-for': this.mockAddress
        }
    }, function optionalCallback(err, httpResponse, body) {
        if (!err) {
            var jsonBody = JSON.parse(body);
            self.jobMap[jsonBody._id] = false;
        } else {
            console.log(err);
        }
        callback(httpResponse.statusCode)
    });

};

module.exports = MockClient;