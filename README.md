# dupont-boyle-simulator
### What's Happening?
* This project is meant to be run in conjunction with dupont-boyle-backend
* When run, it spins up mocked out (but fully functional) instances of clients and gear.
* The clients will randomly upload jobs to be processed, and poll for their status until they complete.
* The gear instances will download jobs that are scheduled to them, process them using digital simulations of analog effects, and upload the process files back to the server.  In an effort to keep file transfers and processing to a minimum, we are using a very short audio file as a seed.  However, all jobs have a hardcoded minimum processing time of 14 seconds to more accurately represent a real-world scenario.
    * NOTE - the processing is done using a local copy of [SoX](http://sox.sourceforge.net), which we have only tested on Macs running 10.11 and up.  If this doesn't run on your machine, you can set `SHOULD_PROCESS_AUDIO` to `false` in `/app/model/mock_gear.js`.
* As a bonus, this simulator has its own HTTP API, which can be used to view the status of all of the clients and gear.  Send a `GET` request to `http://localhost:3002/` for a JSON dump.
    * For an even prettier view, open `/simwatch/simwatch.html` in a browser from the machine on which this simulator runs.
    
### Setting Up
* [Follow this Guide](https://blog.risingstack.com/node-hero-tutorial-getting-started-with-node-js/) to install Node and get a sense for how to start a project and add dependencies.
* Note - this project was built using v6.9.1.  The guide is a little out of date here.
* If it is your first time running the simulator, or your dependencies are otherwise out of date, run `npm install` to get the latest.
* While in the project directory, executing `npm start` will load/run our `index.js` file, which only exists to run our application's "main" file - `/app/index.js`.  This is where the simulator starts.
* Inside `/app/index.js`, we define the `HOST` and `PORT_NO` of the server that this simulator is meant to point at.  This is set to `localhost` and `5000` by default (same as the backend defaults).
* You can also change the simulation parameters from `/app/index.js`:
    * `NUM_CLIENTS` - the number of clients that will upload jobs to be processed. 
    * `NUM_GEAR` - the number of gear instances that can receive and process jobs.
    * `GEAR_LOOP_INTERVAL_MILLIS` - how frequently the gear instances will check for new jobs.
    * `CLIENT_LOOP_INTERVAL_MILLIS` - how frequently the simulated clients upload new jobs and check old jobs for completion.